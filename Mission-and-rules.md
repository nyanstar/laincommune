## Mission statement

**This is a draft and needs inputs from everyone**
**It does not yet represent a consensus**

> The goal of the commune is to maximize the freedom of its members and their ability to realize their personnal projects, be they artistic, technologic or otherwise.
> Any other goal, such as reaching self-sufficiency, is in service of this main goal, and should only be pursued as long and as far as it does contribute toward it.
> The goal of the commune is NOT to make a political statement, NOR to be a social experiment. While it might de facto be one, given that such a life mode is rare in the first world, this should not be accepted as being part of the identity of the commune. No instability should be brought to the commune for the sake of experimenting, only if it contributes toward the main goal. While each member is free to do what he wants on his own, the commune as a whole and its ressources should not take part in any political movment.

> In order to maximize stability, to remove single points of failure, decision making and legal ownership of the commune's land and constructions  should be as distributed amongst the members as possible, to whatever degree is allowed by the local laws. As well as sources of income, knowledge of the essential systems, and so on.
> It is essential to preserve the freedom of each member to leave, thus a member leaving should not impact strongly the commune, so that they do not feel a moral obligation to stay against their will. They should also be able to retrieve their monetary participation within a reasonable lapse of time.

## Proposed rules

     hardline rule 1: if you haven't seen cereal experiments lain at least 5 times you are not allowed in.
     memes asside, just follow the golden rule "do unto others as you would have them do to you". -Kant's golden rule is BS and fails to provide satisfctory outcomes for all involved parties in some instances. Better to form a union of egoists where complete voluntarity is expected and any implicit or explicit violation of that voluntary is grounds for dissolution of that subsection of the union until voluntarity can be reestablished.
     Hardline rule 2 : no idle freeloader
     the commune should be a place of productivity and self improvement, be it artistic, technological, physical... It's fine if you don't earn money but you should have done something with your day by dinner time. -I agree with this.-also agree - I think its important to recognise that sometimes people get sick or can't conform to prevailing conceptions of 'productive' on a daily basis. There are various methods of identifying and handling freeloaders that we can explore and adapt from other commune experiments. There should be a section in which we discuss a method to chronically assess the state of the commune. I will start it ref: productivity (change the title at will)
     - Principle of Non-violence: Violence or threats of violence among members of the commune and unprovoked violence against non-members is not tolerated. This includes violations of consent and hate-speech.=yeah fuck offWe'll need a precise definition of hate speech. - Happy to discuss. Ive been the target of harassment based on gender/sexuality and that's something I want to avoid in the future. "yeah fuck off" thats not very constructive, but thank you for the feedback.
     - Principle of Non-Coercion: Association with the commune is voluntary. Members may withdraw and leave at any time. Members cannot force one another through real or implied leverage. -bt: unless in the defense of yourself or others should likely be added here.
     - Principle of Non-Heirarchy: Members of the community are not ranked or priviledged above other members. -bt: I think heirarchy can sometimes be nice if it reduces the labor of the community. If the community is involved in manufacturing cheese, the cheese workers might not want to think about the amounts of certain cheeses they have to make or how much to sell them for but rather just focus on their craft. By electing a manager the collective gives it's self less work overall because now the rather than every cheese worker thinking of these problems and voting only one person has to. I'd encourage this principle to be changed to no involentary heirarchy.
     - If the commune succeeds to supply each member with what ever we say is its raison de etre (housing food internet etc), productivity is not obligatory. If not, it is. -bt: A good way to manage this might be to have a list of jobs that need to be done a certain amount of time per week, and if any job fails to meet this volunarily jobs are assigned to the non-volunteers evenly. (useing smaller shifts rather than chance to distribute)

## Productivity:
        A twice daily -id suggest weekly instead, because for any sizable projects your just gonna a hear 'yep working on it' every day-gathering of interested members to share personal goals. -bt: agreed. The first to outline what they want to accomplish that day and the second to elaborate on what was actually done. Those who do not conform to majority sleep patterns, or do not have goals they can grok in a way that is expressible in spoken language can submit periodicals at will, to be expressed/interpretted by any individual(s) at the gathering.
     I think aligning community checkins with breakfast and dinner is a good idea. This gives anyone who would like help with a project to put that on your phone the table, and anyone who isnt particularly busy to volunteer time. It also creates a forum for bringing up decisions for group discussion. For larger projects and "state of the commune", finance for example, updates those can be spaced longer apart.


## Minimum living standards


     minimum standards of living, add your own if you disagree :
         xDSL internet connection + enough electricity to power a raspbery pi and a screen 12/7
         individual room / shelter at some point, to get some time alone, especially if far from the city
         warm water : doesn't need a lot with a washcloth rather than a shower
         heating : in the commone room / kitchen and in the bathroom, not necessary in the bedrooms with a quilt thick enough
         A/C : not necessary at all, siesta in the day and work in the night
         food : bread / rice / lentils / eggs / veggies ; go vegetarian or hunt for meat or raise chickens and goats
         water : enough to drink and cook without worry, and to wash now and then ; around 30L/day/person ?
         no soylent

    Minimum living standards-

    dialup or DSL internet connection maybe wifi or Ham radio internet?

    A comfy place to park my van with plenty of sunlight and few plants. Also no mud!

    I use baby wipes to get clean mostly, but I can convert to using local water supplies if available.

    I have a poop bucket, but I need a place to securely and sustainably dispose of my trash and personal waste.

    I like soylent, need a place to have it delivered from forwarding or similiar. I am happy to eat other things if they prove easier and cheaper with comparable nutritional value.

    No dead weight and preferably no addicts that need to be tended to. Dependance is not preferable, though exchanges of mutual benfit certainly are.

    Somekind of lab space please, though not neccessary, I will need a dedicated place that I can organize and trust my projects will be reasonably secure and undisturbed. I am a bit like a skinnier taller mole from the film atlantis when it comes to my work space. " Do not disturb the dirt!"
