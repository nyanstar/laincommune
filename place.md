# Places where to establish the commune

* One in the US, one in the EU
* In a rural setting rather than urban, for much cheaper land to play with

## Proposed countries in Europe

* Slovenia
* France
* Catalonia
* Netherlands
* Portugal - good drug laws
* Bulgaria 
* Ukraine  - great booming tech sector, biggest european country (disregarding russia) = lots of space, cheap

## Proposed states in the USA

* Arizona
* Slab City
* Eureka county Nevada
* Montezuma County 
* if squat: greater nyc area (not nyc obviously)
* Maine
* Alexandria, Indiana

## Property Taxes

for 2017 from https://data.oecd.org/tax/tax-on-property.htm  
Missing lots of countries. Doesn't even have all european, non-EU countries (notably Bulgaria & Romania, which have a low cost of living I presume) but it's a start.

Australia	2.997 (as of 2016)  
Austria	        0.524  
Belgium		3.518  
Canada		3.837  
Chile		1.085  
Czech Republic	0.473  
Denmark		1.822  
Estonia		0.244  
Finland		1.545  
France		4.398  
Germany		1.025  
Greece		3.199  
Hungary		1.071  
Iceland		2.033  
Ireland		1.282  
Israel		3.260  
Italy		2.560  
Japan		2.532  
Korea		3.144  
Latvia		1.014  
Lithuania	0.384  
Luxembourg	3.717  
Mexico		0.315 (as of 2016)  
Netherlands	1.551  
New Zealand	1.915  
Norway		1.266  
OECD - Average	1.944  
Poland		1.206  
Portugal	1.396  
Slovak Republic	0.424  
Slovenia	0.639  
Spain		2.526  
Sweden		0.989  
Switzerland	2.032  
Turkey		1.122  
United Kingdom	4.186  
United States	4.179  

