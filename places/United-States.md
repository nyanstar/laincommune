##TLDR
Based on the [People page](http://commune.muskox.me:4567/People) first and the data collected here second, I believe New York, or Maine are the obvious choices for the location of a US based commune.

##Land Value
![land price](http://farminvestor.com/wp-content/uploads/2017/09/USA-land-prices-per-acre-state.jpeg)
For whatever reason New England's data is aggregated, while not a exact measure we can make a guess of the crop land value based on the [general farm land data provided for by the USDA](https://www.usda.gov/nass/PUBS/TODAYRPT/land0817.pdf):

* Maine $2,200
* Vermont $3,360
* New Hampshire $4,500
* Massachusetts $10,400
* Connecticut $11,200 
* Rhode Island $13,800

Nevada is also not included in the crop data, USDA has its average farm land cost at $1,110 per acre, however crop land in the region is dramatically more expensive due to the abundance of prairie and the lack of precipitation and good soils. A better guess would be around $3,400.

All prices per acre on the map and from the USDA farm land data are just averages, meaning that the actual price for land someone might purchase if they're paying attention is much lower. Even in my limited searches I've found land at around $1000 a acre for around 20 acres in Maine, West Virginia, and Alabama. There is likely land priced at such values in all states with similar averages.

Price per acre often varies with the size of the plot you are looking to purchase with the per acre value dropping with size. This means it might be wise to purchase too much land, but to sell off a tract at the price per acre of smaller plots, making a immediate profit and lowering the cost of land overall.

##Property Taxes
![property taxes](https://upload.wikimedia.org/wikipedia/commons/0/05/Average_county_property_taxes_in_the_United_States.jpg)
Property taxes vary a good bit in the US but are overall quite low. With the addition of a very common exemptions for farmers [where buildings are not counted towards property value for tax purposes](https://www.tax.ny.gov/pubs_and_bulls/orpts/farmbld.htm), taxes will only constitute a extremely small portion of the expenses of the commune in the US. That being said insuring that the area has the necessary tax exemptions and preferably a low overall property tax should be factored into the selection of a location.

Many areas where this tax exemption is available requires the creation of a agricultural business and even a certain amount of gross income for a time. These things tend to be quite easy to achieve though.

Many areas also have a tax exemption from sales tax on items purchased for farm use, while I doubt this would be a dramatic savings, it'd be a savings none the less.

##Water
![precipitation](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Average_precipitation_in_the_lower_48_states_of_the_USA.png/1024px-Average_precipitation_in_the_lower_48_states_of_the_USA.png)
In the western part of the country water is a big deal. While the east uses something called [Riparian water rights](https://en.wikipedia.org/wiki/Riparian_water_rights) which in effect ties the flow of water to the land, the west uses something called [Prior appropriation water rights](https://en.wikipedia.org/wiki/Prior-appropriation_water_rights) which separates ownership of land from that of the water. What this means is that in the west you can buy land without the water rights to either the ground water or the rainfall on your property making it much more difficult to establish the water catchment, wells, and swales necessary to water your crops in this dry region. In addition to the west Texas and Mississippi have Prior appropriation. This isn't a deal breaker but should be considered.

##Soil
![soil quality](https://www.nrcs.usda.gov/Internet/FSE_MEDIA/nrcs142p2_049832.jpg)
This is more of a general map of the quality of soils for agricultural use in the world. This is very useful not only for the united states but for any nation. This is just inherent soil quality though, with effort and money any soil can be made to be world class. Similarly to Prior appropriation this isn't a deal breaker but should be considered. A more higher resolution map can be found [here](https://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/nrcs142p2_050976.zip)

##Conclusion
The following states seem pragmatic to me given the data I've seen:

* Maine
* Vermont
* New York
* West Virginia
* Missouri
* Arkansas
* Louisiana
* Mississippi
* Alabama
* Georgia
* South Carolina
* Tennessee
* Kentucky
* Kansas
* Oklahoma
* Oregon
* Washington
* Idaho

The following states seem like they need further research to go one way or the other:

* North Dakota
* South Dakota
* Utah
* Colorado
* Montana
* Wyoming
* Texas (property taxes higher than equivalent land surrounding and prior appropriation of water)

All in all from what I've seen there are options all over the country, and that when picking between prospective locations our primary concern should be first and foremost proximity to lains, and only secondly the cost & productivity of the land. For this reason the suggestion I'm going to give rather than Oklahoma which is the state which would likely otherwise be the best, would be that we look first at the regions in which most lains interested in the commune live. If it can't be decided through these means alone I'd also suggest we look into the states of non-interested lains, if we can convince appleman or individuals to give us this data. Based on the [People page](http://commune.muskox.me:4567/People) first and the data collected here second, I believe New York, or Maine are the obvious choices for the location of a US based commune.
